# Exercise: Star Wars API ETL
### Using the Star Wars API (https://swapi.dev/)

1. Find the ten characters who appear in the most Star Wars films
2. Sort those ten characters by height in descending order (i.e., tallest first)
3. Produce a CSV with the following columns: name, species, height, appearances
4. Send the CSV to httpbin.org
5. Create automated tests that validate your code


# How to run Star Wars Project with Docker  

### Prerequisite:
- Docker must be installed in your machine ( https://www.docker.com/products/docker-desktop )

### How ? :

1. Create virtual environment (macOS):
    - conda (https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html): 
      - `conda create --name myenv`
      - `conda activate myenv`
    - Virtualenv ( https://docs.python.org/3/library/venv.html )
      - `python3 -m venv myenv`
      - `. myenv/bin/activate`
      
2. Use the Following command to build , run a docker image :

| **Description** | **Make Command (default image name: star-wars:2.0)** | **Docker Command** |
| ------ | ------ |------  | 
| Build docker image | `make build` | `docker build -t <name:versipn> .`|
| Run docker image | `make up` | `docker run <name:version>`|
| Run all automated tests | `make test` | `docker run docker run <name:version> pytest`|
| Shortcut to build and run docker image| `make build-up` | |

### Additional Info

If the following error occur : `ModuleNotFoundError: No module named 'app'`
  - Type the following command in the terminal : `export PYTHONPATH=$PATHONPATH:'pwd'`
  - Or use make command : `make fix-app`

### Output generated from 'data.csv':
![Alt text](screenshot/Screenshot 2022-01-11 at 10.53.53.png)


