FROM python:3.9

WORKDIR /python-str_wars
COPY requirements.txt .

RUN pip install -r requirements.txt
COPY ./app ./app
ENV PYTHONPATH "${PYTHONPATH}:/app"
CMD ["python","./app/main.py"]
