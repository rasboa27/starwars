
.PHONY: test
test: ## Run all the tests
	@echo "Running test ..."
	pytest

.PHONY: build
build: ## Build docker image
	@echo "Building image ..."
	@docker build -t star-wars:2.0 .

.PHONY: fix-app
fix-app: ## Fix app issue
	@export PYTHONPATH=$PATHONPATH:`pwd`

.PHONY: up
up:  ## Run docker image
	@echo "Running image ..."
	@docker run star-wars:2.0


.PHONY: build up
build-up:  build up ## Build and Run docker image

.PHONY: black
black: ## Run black formatters
	@echo "Black formatters ..."
	black ${CURDIR}

.PHONY: isort
isort: ## Run isort formatters
	@echo "isort formatters ..."
	isort .

.PHONY: formatters
formatters: black isort ## Run all formatters