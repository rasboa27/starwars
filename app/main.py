from functions import generate_csv, get_all_characters, post_csv_file, sorting

if __name__ == "__main__":
    all_characters = get_all_characters()
    sorted_characters = sorting(characters_list=all_characters)
    generate_csv(data=sorted_characters, file_name="data")
    post_csv_file(file_name="data")
