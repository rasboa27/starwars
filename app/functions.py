import csv
import math
import os
from typing import Optional, Union

import requests


def get_all_characters() -> list:
    """
    Getting all the characters details
    :return: all characters presents in the database
    :rtype : list
    """
    rq = requests.get(url="http://swapi.dev/api/people/").json()
    characters_list = []
    print("Getting Characters Details...")
    for i in range(1, math.ceil(rq["count"] / 10) + 1):
        url = "http://swapi.dev/api/people/?page=" + str(i)
        characters = requests.get(url=url).json()["results"]
        for character in characters:
            characters_list.append(get_character_details(details=character))
    return characters_list


def get_species(link) -> Optional[str]:
    """
    Get a character specie or species from Star Wars API
    :param link: url/s of the specie
    :type : list
    :return: name of the specie/s
    :rtype : Optional[str]
    """
    species = []
    if link:
        for url in link:
            rq = requests.get(url=url).json()
            species.append(rq["name"])
        return ",".join(species)
    else:
        return


def get_character_details(details) -> dict[str, Union[int, str, None]]:
    """
    Filter a character details
    :param details: character object
    :type : dict
    :return: information about a character
    :rtype : dict[str, Union[int, str, None]
    """
    return {
        "name": details["name"],
        "height": int(details["height"]) if details["height"] != "unknown" else 0,
        "species": get_species(details["species"]),
        "appearance": len(details["films"]),
    }


def sorting(characters_list) -> list:
    """
    Sort ten characters by height in descending order (i.e., tallest first)
    :param characters_list:
    :return: ordered by 10 highest characters by appearances and height
    :rtype: list
    """
    print("Start Sorting...")
    by_appearances = sorted(
        characters_list, key=lambda d: d["appearance"], reverse=True
    )[:10]
    by_highest_height = sorted(by_appearances, key=lambda c: c["height"], reverse=True)
    print("Sorting Completed...")
    return by_highest_height


def generate_csv(data, file_name) -> dict[str, str]:
    """
    Generating a cvv file with all the sorted 10 characters
    :param data: sorted characters list
    :type: list
    :param file_name: the name of the csv file
    :type: str
    :return: status of the generation of cvs file
    :rtype: dict[str, str]
    """
    if data:
        csv_columns = ["name", "height", "species", "appearance"]

        root = os.path.dirname(os.path.abspath("main.py")) + "/"
        csv_file = root + file_name + ".csv"
        try:
            with open(csv_file, "w") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                writer.writeheader()
                for character in data:
                    writer.writerow(character)
            print("CSV File Generated...")
            return {"status": "OK"}

        except IOError:
            print("I/O error")
            return {"status": "Failed"}
    else:
        return {"status": "Failed"}


def post_csv_file(file_name) -> dict:
    """
    Send a csv file to http://httpbin.org/post
    :param file_name : name of the csv file
    :type : str
    :return: response status of request
    :rtype: dict
    """
    root = os.path.dirname(os.path.abspath("main.py")) + "/"
    csv_file = root + file_name + ".csv"
    if os.path.isfile(csv_file):
        files = {"upload_file": open(csv_file, "rb")}
        test_url = "http://httpbin.org/post"
        re = requests.post(url=test_url, files=files)
        print("CSV File Posted to http://httpbin.org ...", re.status_code)
        return {"status": re.status_code}
    else:
        return {"status": "could not send csv"}
