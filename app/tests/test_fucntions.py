import os
from unittest import mock

import pytest

from app.functions import (generate_csv, get_character_details, get_species,
                           post_csv_file, sorting)


@pytest.mark.parametrize(
    "character, species",
    [
        (
            {
                "name": "Luke Skywalker",
                "height": "172",
                "mass": "77",
                "hair_color": "blond",
                "skin_color": "fair",
                "eye_color": "blue",
                "birth_year": "19BBY",
                "gender": "male",
                "homeworld": "",
                "films": [
                    "https:/fake/films/1/",
                    "https:/fake/films/2/",
                    "https:/fake/api/films/3/",
                    "https:/fake/films/6/",
                ],
                "species": [],
                "vehicles": ["", ""],
                "starships": ["", ""],
                "created": "",
                "edited": "",
                "url": "https://swapi.dev/api/people/1/",
            },
            None,
        ),
        (
            {
                "name": "Fake Character",
                "height": "192",
                "mass": "75",
                "hair_color": "black",
                "skin_color": "fair",
                "eye_color": "blue",
                "birth_year": "19BBY",
                "gender": "male",
                "homeworld": "",
                "films": ["https:/fake/films/1/", "https:/fake/films/2/"],
                "species": ["url1", "url2"],
                "vehicles": ["", ""],
                "starships": ["", ""],
                "created": "",
                "edited": "",
                "url": "https:/fake/people/15/",
            },
            "Droid,Human",
        ),
        (
            {
                "name": "Fake Character2",
                "height": "unknown",
                "mass": "5",
                "hair_color": "black",
                "skin_color": "fair",
                "eye_color": "blue",
                "birth_year": "19BBY",
                "gender": "male",
                "homeworld": "",
                "films": ["https:/fake/films/1/", "https:/fake/films/2/"],
                "species": ["url1"],
                "vehicles": ["", ""],
                "starships": ["", ""],
                "created": "",
                "edited": "",
                "url": "https:/fake/people/15/",
            },
            "Human",
        ),
    ],
    ids=[
        "character1 with no species",
        "character2 with species",
        "character3 with no height",
    ],
)
@mock.patch("app.functions.get_species")
def test_get_character_details(mocked_get_species, character, species):
    mocked_get_species.return_value = species
    expected = {
        "name": character["name"],
        "height": int(character["height"]) if character["height"] != "unknown" else 0,
        "species": species,
        "appearance": len(character["films"]),
    }
    assert get_character_details(character) == expected


@pytest.mark.parametrize(
    "url, species",
    [
        (["fake1"], "Droid"),
        (["fake1", "fake2"], "Droid,Droid"),
    ],
    ids=["one species", "two species"],
)
@mock.patch("app.functions.requests.get")
def test_get_species(mock_rq, url, species):
    mock_resp = mock.Mock(
        **{"status_code": 200, "json.return_value": {"name": "Droid"}}
    )
    mock_rq.return_value = mock_resp
    assert get_species(link=url) == species


def generate_user_details(n):
    return {
        "name": "name" + str(n),
        "height": n * 14,
        "species": "species" + str(n),
        "appearance": n,
    }


@pytest.mark.parametrize(
    "characters , expected_sorted_char",
    [
        (
            [(generate_user_details(n=i)) for i in range(20)],
            [(generate_user_details(n=i)) for i in range(19, -1, -1)],
        ),
        (
            [(generate_user_details(n=i)) for i in range(5)],
            [(generate_user_details(n=i)) for i in range(4, -1, -1)],
        ),
    ],
    ids=["sorting with 20 characters", "sorting with 5 characters"],
)
def test_sorting(characters, expected_sorted_char):
    assert sorting(characters_list=characters) == expected_sorted_char[:10]


@pytest.mark.parametrize(
    "data_sorted , file_name , responseGen,responsePost",
    [
        (
            [(generate_user_details(n=i)) for i in range(19, -1, -1)],
            "data1",
            {"status": "OK"},
            {"status": 200},
        ),
        (
            [(generate_user_details(n=i)) for i in range(4, -1, -1)],
            "data2",
            {"status": "OK"},
            {"status": 200},
        ),
        ([], "data3", {"status": "Failed"}, {"status": "Failed"}),
    ],
    ids=[
        "generate/post csv file with 10 entries",
        "generate/post csv file with 5 entries",
        "failed generate/post csv file with 5 entries",
    ],
)
@mock.patch("app.functions.requests.get")
def test_generate_and_post_csv(
    mocked_request, data_sorted, file_name, responseGen, responsePost
):
    generate_csv_file = generate_csv(data=data_sorted, file_name=file_name)
    assert generate_csv_file == responseGen

    if data_sorted:
        mocked_request.get.return_value = {"statatus_code": responsePost["status"]}
        post_csv = post_csv_file(file_name=file_name)
        assert post_csv == responsePost

    # deleting all csv files created

    root = os.path.dirname(os.path.abspath("main.py")) + "/"
    path = root + file_name + ".csv"
    if os.path.isfile(path):
        os.remove(path)
